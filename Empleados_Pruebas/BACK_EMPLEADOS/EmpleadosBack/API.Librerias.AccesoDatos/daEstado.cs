﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using Npgsql;

namespace API.Librerias.AccesoDatos
{
    public class daEstado
    {
        /// <summary>
        /// Método que permite listar los Estado.
        /// </summary>
        public List<beEstado> ListarEstado()
        {
            beEstado obeEstado;

            string query = $@"
                            SELECT id_estado, descripcion_estado, codigo_estado
                            FROM Estado
                            order by id_Estado;                                   
            ";

            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            List<beEstado> ListabeEstado = new List<beEstado>();

            while (dr.Read())
            {
                obeEstado = new beEstado();
                obeEstado.id_estado = dr.GetInt32(0);
                obeEstado.descripcion_estado = dr.GetString(1);
                obeEstado.codigo_estado = dr.GetInt32(2);

                ListabeEstado.Add(obeEstado);
            }
            v_con.Close();
            return ListabeEstado;
        }

        /// <summary>
        /// Método que permite seleccionar un Estado.
        /// </summary>
        public beEstado SeleccionarEstado(int id_Estado)
        {
            string query = $@"
                       (SELECT id_estado, descripcion_estado, codigo_estado
                        FROM Estado
                        where id_Estado = " + id_Estado + " order by id_Estado desc);";

            beEstado obeEstado = new beEstado();
            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            while (dr.Read())
            {
                obeEstado = new beEstado();
                obeEstado.id_estado = dr.GetInt32(0);
                obeEstado.descripcion_estado = dr.GetString(1);
                obeEstado.codigo_estado = dr.GetInt32(2);

            }
            v_con.Close();
            return obeEstado;
        }

    }
}
