﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using Npgsql;

namespace API.Librerias.AccesoDatos
{
    public class daCorreo
    {
        /// <summary>
        /// Método que permite listar los correos.
        /// </summary>
        public List<beCorreo> ListarCorreo()
        {
            beCorreo obeCorreo;

            string query = $@"
                            SELECT id_correo, dominio, descripcion, id_pais, codigo_correo
                            FROM correo
                            order by id_correo;                                   
            ";

            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            List<beCorreo> ListabeCorreo = new List<beCorreo>();

            while (dr.Read())
            {
                obeCorreo = new beCorreo();
                obeCorreo.id_correo = dr.GetInt32(0);
                obeCorreo.dominio = dr.GetString(1);
                obeCorreo.descripcion = dr.GetString(2);
                obeCorreo.id_pais = dr.GetInt32(3);
                obeCorreo.codigo_correo = dr.GetInt32(4);

                ListabeCorreo.Add(obeCorreo);
            }
            v_con.Close();
            return ListabeCorreo;
        }

        /// <summary>
        /// Método que permite seleccionar un correo.
        /// </summary>
        public beCorreo SeleccionarCorreo(int id_pais)
        {
            string query = $@"
                       (SELECT id_correo, dominio, descripcion, id_pais, codigo_correo
                        FROM correo
                        where id_pais = " + id_pais + " order by id_correo desc);";

            beCorreo obeCorreo = new beCorreo();
            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            while (dr.Read())
            {
                obeCorreo = new beCorreo();
                obeCorreo.id_correo = dr.GetInt32(0);
                obeCorreo.dominio = dr.GetString(1);
                obeCorreo.descripcion = dr.GetString(2);
                obeCorreo.id_pais = dr.GetInt32(3);
                obeCorreo.codigo_correo = dr.GetInt32(4);

            }
            v_con.Close();
            return obeCorreo;
        }

    }
}
