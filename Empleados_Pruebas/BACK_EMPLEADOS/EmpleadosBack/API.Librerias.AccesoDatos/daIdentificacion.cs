﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using Npgsql;

namespace API.Librerias.AccesoDatos
{
    public class daIdentificacion
    {
        /// <summary>
        /// Método que permite listar las Identificacion.
        /// </summary>
        public List<beIdentificacion> ListarIdentificacion()
        {
            beIdentificacion obeIdentificacion;

            string query = $@"
                            SELECT id_identificacion, descripcion_identificacion, sigla_identificacion, codigo_identificacion
                            FROM identificacion
                            order by id_identificacion;                                   
            ";

            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            List<beIdentificacion> ListabeIdentificacion = new List<beIdentificacion>();

            while (dr.Read())
            {
                obeIdentificacion = new beIdentificacion();
                obeIdentificacion.id_identificacion = dr.GetInt32(0);
                obeIdentificacion.descripcion_identificacion = dr.GetString(1);
                obeIdentificacion.sigla_identificacion = dr.GetString(2);
                obeIdentificacion.codigo_identificacion = dr.GetInt32(3);

                ListabeIdentificacion.Add(obeIdentificacion);
            }
            v_con.Close();
            return ListabeIdentificacion;
        }

        /// <summary>
        /// Método que permite seleccionar un Identificacion.
        /// </summary>
        public beIdentificacion SeleccionarIdentificacion(int id_area)
        {
            string query = $@"
                       (SELECT id_identificacion, descripcion_identificacion, sigla_identificacion, codigo_identificacion
                        FROM identificacion
                        where id_identificacion = " + id_area + " order by id_Identificacion desc);";

            beIdentificacion obeIdentificacion = new beIdentificacion();
            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            while (dr.Read())
            {
                obeIdentificacion = new beIdentificacion();
                obeIdentificacion.id_identificacion = dr.GetInt32(0);
                obeIdentificacion.descripcion_identificacion = dr.GetString(1);
                obeIdentificacion.sigla_identificacion = dr.GetString(2);
                obeIdentificacion.codigo_identificacion = dr.GetInt32(3);

            }
            v_con.Close();
            return obeIdentificacion;
        }

    }
}
