﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using Npgsql;

namespace API.Librerias.AccesoDatos
{
    public class daEmpleado
    {
        /// <summary>
        /// Método que permite adicionar un empleado.
        /// </summary>
        public int AdicionarEmpleado(beEmpleado obeEmpleado)
        {
            string insert_empleado= @"INSERT INTO public.empleado
                                        (primer_apellido, segundo_apellido, primer_nombre, otros_nombres, 
                                        id_pais, id_identificacion, identificacion_empleado, 
                                        correo_empleado, fecha_ingreso_empleado, id_areaempresa, 
                                        id_estado, fecha_registro_empleado, fecha_modificacion_empleado)

                                        VALUES ('" + obeEmpleado.primer_apellido + "', '" + obeEmpleado.segundo_apellido + "', '" + obeEmpleado.primer_apellido + "', " +
                                        "'" + obeEmpleado.otros_nombres + "', " + obeEmpleado.id_pais + ", " + obeEmpleado.id_identificacion + ", '" + obeEmpleado.identificacion_empleado + "'," +
                                        " '" + obeEmpleado.correo_empelado + "', NOW() , " + obeEmpleado.id_areaempresa + ", " + obeEmpleado.id_estado + ", NOW(), NOW() );";


            int idElemento = -1;

            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();

            NpgsqlCommand comands = new NpgsqlCommand(insert_empleado, v_con);

            int nRegistros = Convert.ToInt32(comands.ExecuteNonQuery());

            if (nRegistros > 0)
            {
                idElemento = nRegistros;
            }
            v_con.Close();
            return idElemento;
        }

        /// <summary>
        /// Método que permite listar los empleados.
        /// </summary>
        public List<beEmpleado> ListarEmpleados()
        {
            beEmpleado obeEmpleado;

            string query = $@"
                       SELECT E.id_empleado, E.primer_apellido, E.segundo_apellido, E.primer_nombre, 
                        E.otros_nombres, E.id_pais, E.id_identificacion, 
                        E.identificacion_empleado, E.correo_empleado, E.fecha_ingreso_empleado, 
                        E.id_areaempresa, E.id_estado, E.fecha_registro_empleado, E.fecha_modificacion_empleado,
                        P.descripcion_pais, I.descripcion_identificacion, AE.descripcion_areaempresa, 
                        ES.descripcion_estado
                        FROM empleado as E
                        join pais as P ON P.id_pais = E.id_pais
                        join identificacion as I ON I.id_identificacion = E.id_identificacion
                        join areaempresa as AE ON AE.id_areaempresa = E.id_areaempresa
                        join estado as ES ON ES.id_estado = E.id_estado order by  E.id_empleado desc              
            ";

            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            List<beEmpleado> ListabeEmpleado = new List<beEmpleado>();

            while (dr.Read())
            {
                obeEmpleado = new beEmpleado();
                obeEmpleado.id_empleado = dr.GetInt32(0);
                obeEmpleado.primer_apellido = dr.GetString(1);
                obeEmpleado.segundo_apellido = dr.GetString(2);
                obeEmpleado.primer_nombre = dr.GetString(3);
                obeEmpleado.otros_nombres = dr.GetString(4);
                obeEmpleado.id_pais = dr.GetInt32(5);
                obeEmpleado.id_identificacion = dr.GetInt32(6);
                obeEmpleado.identificacion_empleado = dr.GetString(7);
                obeEmpleado.correo_empelado = dr.GetString(8);
                obeEmpleado.fecha_ingreso_empleado = dr.GetDateTime(9);
                obeEmpleado.id_areaempresa = dr.GetInt32(10);
                obeEmpleado.id_estado = dr.GetInt32(11);
                obeEmpleado.fecha_registro_empleado = dr.GetDateTime(12);
                obeEmpleado.fecha_modificacion_empleado = dr.GetDateTime(13);
                obeEmpleado.descripcion_pais = dr.GetString(14);
                obeEmpleado.descripcion_identificacion = dr.GetString(15);
                obeEmpleado.descripcion_areaempresa = dr.GetString(16);
                obeEmpleado.descripcion_estado = dr.GetString(17);

                ListabeEmpleado.Add(obeEmpleado);
            }
            v_con.Close();
            return ListabeEmpleado;
        }

        /// <summary>
        /// Método que permite seleccionar un empleado.
        /// </summary>
        public beEmpleado SeleccionarEmpleado(int id_empleado)
        {
            string query = $@"
                       (SELECT E.id_empleado, E.primer_apellido, E.segundo_apellido, E.primer_nombre, 
                        E.otros_nombres, E.id_pais, E.id_identificacion, 
                        E.identificacion_empleado, E.correo_empleado, E.fecha_ingreso_empleado, 
                        E.id_areaempresa, E.id_estado, E.fecha_registro_empleado, E.fecha_modificacion_empleado,
                        P.descripcion_pais, I.descripcion_identificacion, AE.descripcion_areaempresa, 
                        ES.descripcion_estado
                        FROM empleado as E
                        join pais as P ON P.id_pais = E.id_pais
                        join identificacion as I ON I.id_identificacion = E.id_identificacion
                        join areaempresa as AE ON AE.id_areaempresa = E.id_areaempresa
                        join estado as ES ON ES.id_estado = E.id_estado    
                        where E.id_empleado = " + id_empleado +")";

            beEmpleado obeEmpleado = new beEmpleado();
            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            while (dr.Read())
            {
                obeEmpleado = new beEmpleado();
                obeEmpleado.id_empleado = dr.GetInt32(0);
                obeEmpleado.primer_apellido = dr.GetString(1);
                obeEmpleado.segundo_apellido = dr.GetString(2);
                obeEmpleado.primer_nombre = dr.GetString(3);
                obeEmpleado.otros_nombres = dr.GetString(4);
                obeEmpleado.id_pais = dr.GetInt32(5);
                obeEmpleado.id_identificacion = dr.GetInt32(6);
                obeEmpleado.identificacion_empleado = dr.GetString(7);
                obeEmpleado.correo_empelado = dr.GetString(8);
                obeEmpleado.fecha_ingreso_empleado = dr.GetDateTime(9);
                obeEmpleado.id_areaempresa = dr.GetInt32(10);
                obeEmpleado.id_estado = dr.GetInt32(11);
                obeEmpleado.fecha_registro_empleado = dr.GetDateTime(12);
                obeEmpleado.fecha_modificacion_empleado = dr.GetDateTime(13);
                obeEmpleado.descripcion_pais = dr.GetString(14);
                obeEmpleado.descripcion_identificacion = dr.GetString(15);
                obeEmpleado.descripcion_areaempresa = dr.GetString(16);
                obeEmpleado.descripcion_estado = dr.GetString(17);

            }
            v_con.Close();
            return obeEmpleado;
        }

        /// <summary>
        /// Método que permite editar un empleado.
        /// </summary>
        public int EditarEmpleado(beEmpleado obeEmpleado)
        {

            string update_empleados = $@" UPDATE public.empleado
                                            SET primer_apellido='" + obeEmpleado.primer_apellido + "', segundo_apellido='" + obeEmpleado.segundo_apellido + "', primer_nombre='" + obeEmpleado.primer_nombre + "', otros_nombres='" + obeEmpleado.otros_nombres + "', id_pais=" + obeEmpleado.id_pais + ", " +
                                            "id_identificacion = " + obeEmpleado.id_identificacion + ", identificacion_empleado ='" + obeEmpleado.identificacion_empleado + "', correo_empleado='" + obeEmpleado.correo_empelado + "', " +
                                            "id_areaempresa=" + obeEmpleado.id_areaempresa + ", id_estado=" + obeEmpleado.id_estado + ", fecha_modificacion_empleado =  NOW()  WHERE id_empleado=" + obeEmpleado.id_empleado + ";";

            int idElemento = -1;

            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();

            NpgsqlCommand comands = new NpgsqlCommand(update_empleados, v_con);

            int nRegistros = Convert.ToInt32(comands.ExecuteNonQuery());

            if (nRegistros > 0)
            {
                idElemento = nRegistros;
            }
            v_con.Close();
            return idElemento;
        }

        /// <summary>
        /// Método que permite eliminar un empleado.
        /// </summary>
        public int EliminaEmpleado(int id_empleado)
        {
            string delete_empleado = $@"DELETE FROM empleado WHERE id_empleado=" + id_empleado + ";";

            int idElemento = -1;

            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();

            NpgsqlCommand v_cmd = new NpgsqlCommand(delete_empleado, v_con);
            int nRegistros = Convert.ToInt32(v_cmd.ExecuteNonQuery());

            if (nRegistros > 0)
            {
                idElemento = nRegistros;
            }
            v_con.Close();
            return idElemento;
        }
    }
}
