﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using Npgsql;

namespace API.Librerias.AccesoDatos
{
    public class daAreaEmpresa
    {
        /// <summary>
        /// Método que permite listar las AreaEmpresa.
        /// </summary>
        public List<beAreaEmpresa> ListarAreaEmpresa()
        {
            beAreaEmpresa obeAreaEmpresa;

            string query = $@"
                            SELECT id_areaempresa, descripcion_areaempresa, sigla_areaempresa, codigo_areaempresa
                            FROM areaempresa
                            order by id_areaempresa;                                   
            ";

            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            List<beAreaEmpresa> ListabeAreaEmpresa = new List<beAreaEmpresa>();

            while (dr.Read())
            {
                obeAreaEmpresa = new beAreaEmpresa();
                obeAreaEmpresa.id_areaempresa = dr.GetInt32(0);
                obeAreaEmpresa.descripcion_areaempresa = dr.GetString(1);
                obeAreaEmpresa.sigla_areaempresa = dr.GetString(2);
                obeAreaEmpresa.codigo_areaempresa = dr.GetInt32(3);

                ListabeAreaEmpresa.Add(obeAreaEmpresa);
            }
            v_con.Close();
            return ListabeAreaEmpresa;
        }

        /// <summary>
        /// Método que permite seleccionar un AreaEmpresa.
        /// </summary>
        public beAreaEmpresa SeleccionarAreaEmpresa(int id_area)
        {
            string query = $@"
                       (SELECT id_areaempresa, descripcion_areaempresa, sigla_areaempresa, codigo_areaempresa
                        FROM areaempresa
                        where id_areaempresa = " + id_area + " order by id_AreaEmpresa desc);";

            beAreaEmpresa obeAreaEmpresa = new beAreaEmpresa();
            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            while (dr.Read())
            {
                obeAreaEmpresa = new beAreaEmpresa();
                obeAreaEmpresa.id_areaempresa = dr.GetInt32(0);
                obeAreaEmpresa.descripcion_areaempresa = dr.GetString(1);
                obeAreaEmpresa.sigla_areaempresa = dr.GetString(2);
                obeAreaEmpresa.codigo_areaempresa = dr.GetInt32(3);

            }
            v_con.Close();
            return obeAreaEmpresa;
        }

    }
}
