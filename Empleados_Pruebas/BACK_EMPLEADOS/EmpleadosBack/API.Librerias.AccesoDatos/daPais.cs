﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using Npgsql;

namespace API.Librerias.AccesoDatos
{
    public class daPais
    {
        /// <summary>
        /// Método que permite listar los Pais.
        /// </summary>
        public List<bePais> ListarPais()
        {
            bePais obePais;

            string query = $@"
                            SELECT id_pais, descripcion_pais, codigo_pais
                            FROM Pais
                            order by id_Pais;                                   
            ";

            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            List<bePais> ListabePais = new List<bePais>();

            while (dr.Read())
            {
                obePais = new bePais();
                obePais.id_pais = dr.GetInt32(0);
                obePais.descripcion_pais = dr.GetString(1);
                obePais.codigo_pais = dr.GetInt32(2);

                ListabePais.Add(obePais);
            }
            v_con.Close();
            return ListabePais;
        }

        /// <summary>
        /// Método que permite seleccionar un Pais.
        /// </summary>
        public bePais SeleccionarPais(int id_pais)
        {
            string query = $@"
                       (SELECT id_pais, descripcion_pais, codigo_pais
                        FROM Pais
                        where id_pais = " + id_pais + " order by id_Pais desc);";

            bePais obePais = new bePais();
            ConexionDB con = new ConexionDB();
            NpgsqlConnection v_con = con.Conexion();
            v_con.Open();
            NpgsqlCommand v_cmd = new NpgsqlCommand(query, v_con);

            using NpgsqlDataReader dr = v_cmd.ExecuteReader();

            while (dr.Read())
            {
                obePais = new bePais();
                obePais.id_pais = dr.GetInt32(0);
                obePais.descripcion_pais = dr.GetString(1);
                obePais.codigo_pais = dr.GetInt32(2);

            }
            v_con.Close();
            return obePais;
        }

    }
}
