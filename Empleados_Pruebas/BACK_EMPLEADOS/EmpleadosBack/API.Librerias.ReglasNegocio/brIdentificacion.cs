﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.AccesoDatos;
namespace API.Librerias.ReglasNegocio
{
    public class brIdentificacion
    {
        public List<beIdentificacion> ListarIdentificacion()
        {
            List<beIdentificacion> obIdentificacion = new List<beIdentificacion>();
            try
            {
                daIdentificacion odaIdentificacion = new daIdentificacion();
                obIdentificacion = odaIdentificacion.ListarIdentificacion();
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obIdentificacion;
        }

        public beIdentificacion SeleccionarIdentificacion(int id_area)
        {
            beIdentificacion obeIdentificacion = new beIdentificacion();
            try
            {
                daIdentificacion odaIdentificacion = new daIdentificacion();
                obeIdentificacion = odaIdentificacion.SeleccionarIdentificacion(id_area);
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obeIdentificacion;
        }

 
    }
}
