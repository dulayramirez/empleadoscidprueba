﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.AccesoDatos;
namespace API.Librerias.ReglasNegocio
{
    public class brAreaEmpresa
    {
        public List<beAreaEmpresa> ListarAreaEmpresa()
        {
            List<beAreaEmpresa> obAreaEmpresa = new List<beAreaEmpresa>();
            try
            {
                daAreaEmpresa odaAreaEmpresa = new daAreaEmpresa();
                obAreaEmpresa = odaAreaEmpresa.ListarAreaEmpresa();
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obAreaEmpresa;
        }

        public beAreaEmpresa SeleccionarAreaEmpresa(int id_pais)
        {
            beAreaEmpresa obeAreaEmpresa = new beAreaEmpresa();
            try
            {
                daAreaEmpresa odaAreaEmpresa = new daAreaEmpresa();
                obeAreaEmpresa = odaAreaEmpresa.SeleccionarAreaEmpresa(id_pais);
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obeAreaEmpresa;
        }

 
    }
}
