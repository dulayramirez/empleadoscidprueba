﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.AccesoDatos;
namespace API.Librerias.ReglasNegocio
{
    public class brEmpleado
    {
    
        public int AdicionarEmpleado(beEmpleado obeEmpleado)
        {
            int idEmpleado = -1;
            try
            {
                daEmpleado odaEmpleado = new daEmpleado();
                idEmpleado = odaEmpleado.AdicionarEmpleado(obeEmpleado);
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return idEmpleado;
        }

        public List<beEmpleado> ListarEmpleados()
        {
            List<beEmpleado> obEmpleado = new List<beEmpleado>();
            try
            {
                daEmpleado odaEmpleado = new daEmpleado();
                obEmpleado = odaEmpleado.ListarEmpleados();
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obEmpleado;
        }

        public beEmpleado SeleccionarEmpleado(int id_empleado)
        {
            beEmpleado obeEmpleado = new beEmpleado();
            try
            {
                daEmpleado odaEmpleado = new daEmpleado();
                obeEmpleado = odaEmpleado.SeleccionarEmpleado(id_empleado);
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obeEmpleado;
        }

        public int EditarEmpleado(beEmpleado obeEmpleado)
        {
            int idEmpleado = -1;
            try
            {
                daEmpleado odaEmpleado = new daEmpleado();
                idEmpleado = odaEmpleado.EditarEmpleado(obeEmpleado);
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return idEmpleado;
        }

        public int EliminaEmpleado(int id_empleado)
        {
            int id_Empleado = -1;
            try
            {
                daEmpleado odaEmpleado = new daEmpleado();
                id_Empleado = odaEmpleado.EliminaEmpleado(id_empleado);
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return id_Empleado;
        }

    }
}
