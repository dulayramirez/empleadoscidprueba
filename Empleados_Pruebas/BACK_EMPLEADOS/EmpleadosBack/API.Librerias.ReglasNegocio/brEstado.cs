﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.AccesoDatos;
namespace API.Librerias.ReglasNegocio
{
    public class brEstado
    {
        public List<beEstado> ListarEstado()
        {
            List<beEstado> obEstado = new List<beEstado>();
            try
            {
                daEstado odaEstado = new daEstado();
                obEstado = odaEstado.ListarEstado();
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obEstado;
        }

        public beEstado SeleccionarEstado(int id_pais)
        {
            beEstado obeEstado = new beEstado();
            try
            {
                daEstado odaEstado = new daEstado();
                obeEstado = odaEstado.SeleccionarEstado(id_pais);
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obeEstado;
        }

 
    }
}
