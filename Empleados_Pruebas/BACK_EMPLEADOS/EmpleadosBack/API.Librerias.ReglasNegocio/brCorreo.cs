﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.AccesoDatos;
namespace API.Librerias.ReglasNegocio
{
    public class brCorreo
    {
        public List<beCorreo> ListarCorreo()
        {
            List<beCorreo> obCorreo = new List<beCorreo>();
            try
            {
                daCorreo odaCorreo = new daCorreo();
                obCorreo = odaCorreo.ListarCorreo();
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obCorreo;
        }

        public beCorreo SeleccionarCorreo(int id_pais)
        {
            beCorreo obeCorreo = new beCorreo();
            try
            {
                daCorreo odaCorreo = new daCorreo();
                obeCorreo = odaCorreo.SeleccionarCorreo(id_pais);
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obeCorreo;
        }

 
    }
}
