﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.AccesoDatos;
namespace API.Librerias.ReglasNegocio
{
    public class brPais
    {
        public List<bePais> ListarPais()
        {
            List<bePais> obPais = new List<bePais>();
            try
            {
                daPais odaPais = new daPais();
                obPais = odaPais.ListarPais();
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obPais;
        }

        public bePais SeleccionarPais(int id_pais)
        {
            bePais obePais = new bePais();
            try
            {
                daPais odaPais = new daPais();
                obePais = odaPais.SeleccionarPais(id_pais);
            }
            catch (Exception ex)
            {
                //GrabarLog(ex.Message, ex.StackTrace);
            }
            return obePais;
        }

 
    }
}
