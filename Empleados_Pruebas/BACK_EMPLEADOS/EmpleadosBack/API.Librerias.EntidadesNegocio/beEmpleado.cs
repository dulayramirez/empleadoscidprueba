﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Librerias.EntidadesNegocio
{
    public class beEmpleado
    {
        public int id_empleado { get; set; }
        public string primer_apellido { get; set; }
        public string segundo_apellido { get; set; }
        public string primer_nombre { get; set; }
        public string otros_nombres { get; set; }
        public int id_pais { get; set; }
        public int id_identificacion { get; set; }
        public string identificacion_empleado { get; set; }
        public string correo_empelado { get; set; }
        public DateTimeOffset fecha_ingreso_empleado { get; set; }
        public DateTimeOffset fecha_modificacion_empleado { get; set; }        
        public int id_areaempresa { get; set; }
        public int id_estado { get; set; }
        public DateTimeOffset fecha_registro_empleado { get; set; }
        public string descripcion_pais { get; set; }
        public string descripcion_estado { get; set; }
        public string descripcion_areaempresa { get; set; }
        public string descripcion_identificacion { get; set; }



    }
    
}
