﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Librerias.EntidadesNegocio
{
    public class beAreaEmpresa
    {
        public int id_areaempresa { get; set; }
        public string descripcion_areaempresa { get; set; }
        public string sigla_areaempresa { get; set; }
        public int codigo_areaempresa { get; set; }

    }
    
}
