﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Librerias.EntidadesNegocio
{
    public class beIdentificacion
    {
        public int id_identificacion { get; set; }
        public string descripcion_identificacion { get; set; }
        public string sigla_identificacion { get; set; }
        public int codigo_identificacion { get; set; }

    }
    
}
