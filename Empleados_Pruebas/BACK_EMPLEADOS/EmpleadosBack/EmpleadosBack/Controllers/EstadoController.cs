﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.ReglasNegocio;
using Microsoft.AspNetCore.Authorization;
using bcpass = BCrypt.Net.BCrypt;

namespace EstadosBack.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class EstadoController : ControllerBase
    {

        [HttpGet, Route("ListarEstado")]
        public List<beEstado> ListarEstado()
        {
            brEstado obrEstado = new brEstado();
            List<beEstado> obeEstado = new List<beEstado>();
            obeEstado = obrEstado.ListarEstado();
            return obeEstado;
        }

        [HttpGet, Route("SeleccionarEstado")]
        public beEstado SeleccionarEstado(int id_Estado)
        {
            brEstado obrEstado = new brEstado();
            beEstado obEstado = new beEstado();
            obEstado = obrEstado.SeleccionarEstado(id_Estado);
            return obEstado;
        }

    }

}
