﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.ReglasNegocio;
using Microsoft.AspNetCore.Authorization;
using bcpass = BCrypt.Net.BCrypt;

namespace PaissBack.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class PaisController : ControllerBase
    {

        [HttpGet, Route("ListarPais")]
        public List<bePais> ListarPais()
        {
            brPais obrPais = new brPais();
            List<bePais> obePais = new List<bePais>();
            obePais = obrPais.ListarPais();
            return obePais;
        }

        [HttpGet, Route("SeleccionarPais")]
        public bePais SeleccionarPais(int id_Pais)
        {
            brPais obrPais = new brPais();
            bePais obPais = new bePais();
            obPais = obrPais.SeleccionarPais(id_Pais);
            return obPais;
        }

    }

}
