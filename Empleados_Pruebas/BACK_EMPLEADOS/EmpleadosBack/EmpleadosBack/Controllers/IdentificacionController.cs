﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.ReglasNegocio;
using Microsoft.AspNetCore.Authorization;
using bcpass = BCrypt.Net.BCrypt;

namespace IdentificacionsBack.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class IdentificacionController : ControllerBase
    {

        [HttpGet, Route("ListarIdentificacion")]
        public List<beIdentificacion> ListarIdentificacion()
        {
            brIdentificacion obrIdentificacion = new brIdentificacion();
            List<beIdentificacion> obeIdentificacion = new List<beIdentificacion>();
            obeIdentificacion = obrIdentificacion.ListarIdentificacion();
            return obeIdentificacion;
        }

        [HttpGet, Route("SeleccionarIdentificacion")]
        public beIdentificacion SeleccionarIdentificacion(int id_Identificacion)
        {
            brIdentificacion obrIdentificacion = new brIdentificacion();
            beIdentificacion obIdentificacion = new beIdentificacion();
            obIdentificacion = obrIdentificacion.SeleccionarIdentificacion(id_Identificacion);
            return obIdentificacion;
        }

    }

}
