﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.ReglasNegocio;
using Microsoft.AspNetCore.Authorization;
using bcpass = BCrypt.Net.BCrypt;

namespace AreaEmpresasBack.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AreaEmpresaController : ControllerBase
    {

        [HttpGet, Route("ListarAreaEmpresa")]
        public List<beAreaEmpresa> ListarAreaEmpresa()
        {
            brAreaEmpresa obrAreaEmpresa = new brAreaEmpresa();
            List<beAreaEmpresa> obeAreaEmpresa = new List<beAreaEmpresa>();
            obeAreaEmpresa = obrAreaEmpresa.ListarAreaEmpresa();
            return obeAreaEmpresa;
        }

        [HttpGet, Route("SeleccionarAreaEmpresa")]
        public beAreaEmpresa SeleccionarAreaEmpresa(int id_AreaEmpresa)
        {
            brAreaEmpresa obrAreaEmpresa = new brAreaEmpresa();
            beAreaEmpresa obAreaEmpresa = new beAreaEmpresa();
            obAreaEmpresa = obrAreaEmpresa.SeleccionarAreaEmpresa(id_AreaEmpresa);
            return obAreaEmpresa;
        }

    }

}
