﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.ReglasNegocio;
using Microsoft.AspNetCore.Authorization;
using bcpass = BCrypt.Net.BCrypt;

namespace EmpleadosBack.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class EmpleadoController : ControllerBase
    {

        [HttpPost, Route("AdicionarEmpleado")]
        public string AdicionarEmpleado(beEmpleado obeEmpleado)
        {
            string Mensaje = "";
            int Respuestas = 0;

            brEmpleado obrEmpleado = new brEmpleado();

            Respuestas = obrEmpleado.AdicionarEmpleado(obeEmpleado);

            if (Respuestas < 1)
            {
                Mensaje = "Fallo";
            }
            return Mensaje;
        }

        [HttpGet, Route("ListarEmpleados")]
        public List<beEmpleado> ListarEmpleados()
        {
            brEmpleado obrEmpleado = new brEmpleado();
            List<beEmpleado> obeEmpleado = new List<beEmpleado>();
            obeEmpleado = obrEmpleado.ListarEmpleados();
            return obeEmpleado;
        }

        [HttpGet, Route("SeleccionarEmpleado")]
        public beEmpleado SeleccionarEmpleado(int id_empleado)
        {
            brEmpleado obrEmpleado = new brEmpleado();
            beEmpleado obEmpleado = new beEmpleado();
            obEmpleado = obrEmpleado.SeleccionarEmpleado(id_empleado);
            return obEmpleado;
        }

        [HttpPut, Route("EditarEmpleado")]
        public string EditarEmpleado(beEmpleado obeEmpleado)
        {
            string Mensaje = "";
            int Respuestas = 0;

            brEmpleado obrEmpleado = new brEmpleado();

            Respuestas = obrEmpleado.EditarEmpleado(obeEmpleado);

            if (Respuestas < 1)
            {
                Mensaje = "Fallo";
            }

            return Mensaje;
        }

        [HttpDelete, Route("EliminaEmpleado")]
        public int EliminaEmpleado(int id_empleado)
        {

            brEmpleado obrEmpleado = new brEmpleado();
            int EmpleadoEliminar = 0;
            EmpleadoEliminar = obrEmpleado.EliminaEmpleado(id_empleado);
            return EmpleadoEliminar;
        }

    }

}
