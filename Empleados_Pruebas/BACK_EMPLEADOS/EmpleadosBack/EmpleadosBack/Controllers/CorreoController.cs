﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Librerias.EntidadesNegocio;
using API.Librerias.ReglasNegocio;
using Microsoft.AspNetCore.Authorization;
using bcpass = BCrypt.Net.BCrypt;

namespace CorreosBack.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class CorreoController : ControllerBase
    {

        [HttpGet, Route("ListarCorreo")]
        public List<beCorreo> ListarCorreo()
        {
            brCorreo obrCorreo = new brCorreo();
            List<beCorreo> obeCorreo = new List<beCorreo>();
            obeCorreo = obrCorreo.ListarCorreo();
            return obeCorreo;
        }

        [HttpGet, Route("SeleccionarCorreo")]
        public beCorreo SeleccionarCorreo(int id_Correo)
        {
            brCorreo obrCorreo = new brCorreo();
            beCorreo obCorreo = new beCorreo();
            obCorreo = obrCorreo.SeleccionarCorreo(id_Correo);
            return obCorreo;
        }

    }

}
