export interface beIdentificacion{
    
    id_identificacion : number; 
    descripcion_identificacion : string;
    sigla_identificacion : string;
    codigo_identificacion : number;
    
}