export interface beEmpleado{
    
    id_empleado : number; 
    primer_apellido : string; 
    segundo_apellido : string;  
    primer_nombre : string;
    otros_nombres: string;
    id_pais : number; 
    id_identificacion : number;
    identificacion_empleado : string; 
    correo_empelado : string;  
    fecha_ingreso_empleado : string;
    fecha_modificacion_empleado: string;
    id_areaempresa : number; 
    id_estado : number; 
    fecha_registro_empleado : string;  
    descripcion_pais : string;
    descripcion_estado: string; 
    descripcion_areaempresa : string;
    descripcion_identificacion: string;

}