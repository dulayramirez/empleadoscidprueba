export { beAreaEmpresa } from './beAreaEmpresa';
export { beCorreo } from './beCorreo';
export { beEmpleado } from './beEmpleado';
export { beEstado } from './beEstado';
export { beIdentificacion } from './beIdentificacion';
export { bePais } from './bePais';