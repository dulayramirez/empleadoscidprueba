import { Component, OnInit, Input } from '@angular/core';
import { FormGroup,FormBuilder, Validators, NgForm, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { beIdentificacion, beEstado, beEmpleado, beCorreo, beAreaEmpresa} from 'src/app/models/interfaces.index';
import { IdentificacionService, EmpleadoService, CorreosService, AreaEmpresaService, PaisService, EstadoService} from 'src/app/services/services.index';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  
  public NombreApp = 'Empleados';
  public year = date.getFullYear();

  form : FormGroup;  
  paginacion : number = 1;
  tipoIdentificacion : any;
  tipoCorreo : any;
  tipoAreaEmpresa : any;
  tipoEstado : any;
  tipoPais : any;
  listaEmpleados : any;
  listaEmpleado : any;
  idTipoIdentificacion : number;
  valueCorreo : string;
  valueCorreos : string;
  valueNombre : string;
  valueAplellido : string;

  constructor (private formBuilder : FormBuilder, private identificacionservice : IdentificacionService, 
              private empleadoservice : EmpleadoService, private correosservice : CorreosService, 
              private areaEmpresaservice : AreaEmpresaService, private paisservice : PaisService, 
              private estadoservice : EstadoService, private toastr: ToastrService) { 

    this.form = this.formBuilder.group({         
      primerApellido : [''],                 
      segundoApellido : [''],                
      primerNombre : [''],                
      otrosNombres : [''],
      idIdentificacion : [''],
      idPais : [''],
      idAreaEmpresa : [''],
      idestados : [''],
      identificacionEmpleado : [''],
      idEmpleado: [{value :'', disabled:true}],
      correosEmpleados : [{value :'', disabled:true}]
    }); 
    
  }

  ngOnInit(): void {

    this.obtenerIdentificacion();
    this.obtenerPais();
    this.obtenerAreaEmpresa();
    this.obtenerEstado();
    this.obtenerAreaEmpleados();

  }

  actualizarControles(){

    this.form = this.formBuilder.group({         
      primerApellido : [''],                 
      segundoApellido : [''],                
      primerNombre : [''],                
      otrosNombres : [''],
      idIdentificacion : [''],
      idPais : [''],
      idAreaEmpresa : [''],
      idestados : [''],
      identificacionEmpleado : [''],
      idEmpleado: [{value :'', disabled:true}],
      correosEmpleados : [{value :'', disabled:true}]
    }); 

  }

  obtenerAreaEmpresa(){

    this.areaEmpresaservice.getAreaEmpresas().subscribe(
      data => {
        this.tipoAreaEmpresa = data;
      },
      err => {
        console.log(err);
      }
    );

  }

  obtenerEstado(){

    this.estadoservice.getEstados().subscribe(
      data => {
        this.tipoEstado = data;
      },
      err => {
        console.log(err);
      }
    );

  }

  obtenerIdentificacion(){

    this.identificacionservice.getIdentificacioniones().subscribe(
      data => {
        this.tipoIdentificacion = data;
      },
      err => {
        console.log(err);
      }
    );

  }

  obtenerAreaEmpleados(){

    this.empleadoservice.getEmpleados().subscribe(
      data => {
        this.listaEmpleados = data;
      },
      err => {
        console.log(err);
      }
    );

  }

  obtenerPais(){

    this.paisservice.getPaises().subscribe(
      data => {
        this.tipoPais = data;
      },
      err => {
        console.log(err);
      }
    );

  }

  registrarEmpleado(){

    const postEmpleados = {
      primer_apellido: this.form.get('primerApellido')?.value, 
      segundo_apellido: this.form.get('segundoApellido')?.value, 
      primer_nombre: this.form.get('primerNombre')?.value, 
      otros_nombres: this.form.get('otrosNombres')?.value, 
      id_pais: this.form.get('idPais')?.value, 
      id_identificacion: this.form.get('idIdentificacion')?.value, 
      identificacion_empleado: this.form.get('identificacionEmpleado')?.value, 
      correo_empelado: this.form.get('correosEmpleados')?.value, 
      id_areaempresa: this.form.get('idAreaEmpresa')?.value, 
      id_estado: this.form.get('idestados')?.value, 
    } 

    this.empleadoservice.postEmpleado(postEmpleados).subscribe(
      data => {
        this.toastr.success('Empleado registrado', 'Registro OK'); 
        this.obtenerAreaEmpleados();
        this.actualizarControles();
      }
    );

  }

  seleccionarEmpleado(valueIdEmpleado : number){

    this.empleadoservice.getEmpleado(valueIdEmpleado).subscribe(
      data => {
        this.listaEmpleado = data;

        this.form.patchValue({      
          primerApellido : this.listaEmpleado.primer_apellido,
          segundoApellido : this.listaEmpleado.segundo_apellido,
          primerNombre: this.listaEmpleado.primer_nombre,
          otrosNombres : this.listaEmpleado.otros_nombres,
          idIdentificacion : this.listaEmpleado.id_identificacion,
          idPais : this.listaEmpleado.id_pais,
          idAreaEmpresa : this.listaEmpleado.id_areaempresa,
          idestados : this.listaEmpleado.id_estado,
          identificacionEmpleado : this.listaEmpleado.identificacion_empleado,
          idEmpleado : this.listaEmpleado.id_empleado,
          correosEmpleados : this.listaEmpleado.correo_empelado
        });

      }
    );

  }

  actualizarEmpleado(){

    const putEmpleados = {
      primer_apellido: this.form.get('primerApellido')?.value, 
      segundo_apellido: this.form.get('segundoApellido')?.value, 
      primer_nombre: this.form.get('primerNombre')?.value, 
      otros_nombres: this.form.get('otrosNombres')?.value, 
      id_pais: this.form.get('idPais')?.value, 
      id_identificacion: this.form.get('idIdentificacion')?.value, 
      identificacion_empleado: this.form.get('identificacionEmpleado')?.value, 
      correo_empelado: this.form.get('correosEmpleados')?.value, 
      id_areaempresa: this.form.get('idAreaEmpresa')?.value, 
      id_estado: this.form.get('idestados')?.value, 
      id_empleado: this.form.get('idEmpleado')?.value, 
    } 

    this.empleadoservice.putEmpleado(putEmpleados).subscribe(
      data => {
        this.toastr.success('Empleado Actualizado', 'Registro OK'); 
        this.obtenerAreaEmpleados();
        this.actualizarControles();
      }
    );
  }

  eliminarEmpleado(valueIdEmpleado : number){
    this.empleadoservice.deleteEmpleado(valueIdEmpleado).subscribe(
      data => {
        this.toastr.success('Empleado Eliminado', 'Registro OK'); 
        this.obtenerAreaEmpleados();
        this.actualizarControles();
      }
    );
  }

  validarCorreo(valueIdCorreo : number){

    this.valueNombre = this.form.controls.primerNombre.value;
    this.valueAplellido = this.form.controls.primerApellido.value;

    if(this.valueNombre == "" || this.valueAplellido == "" ){
    this.toastr.error('Complete los datos', 'Primer Nombre y Primer Apellido');
    this.form = this.formBuilder.group({         
      idPais : ['']
    }); 
    }else{
      this.correosservice.getCorreo(valueIdCorreo).subscribe(
        data => {
          this.tipoCorreo = data;
          this.valueCorreos =  this.valueNombre + "." + this.valueAplellido + this.tipoCorreo.dominio;

          for (let i = 0; i < this.listaEmpleados.length; i++) {

            if(this.listaEmpleados[i].correo_empelado == this.valueCorreos){     
              this.valueCorreos =  this.valueNombre + "." + this.valueAplellido + "." + [i] + this.tipoCorreo.dominio;
              this.valueCorreo = this.valueCorreos;
              this.form.controls.correosEmpleados.patchValue(this.valueCorreo);
            }else{
              this.valueCorreo = this.valueCorreos;
              this.form.controls.correosEmpleados.patchValue(this.valueCorreo);
            }   
        }

        },
        err => {
          console.log(err);
        }
      );
    }

  }

  validarIdentificacion(valueIdentificacion : string){
    
    this.idTipoIdentificacion = this.form.controls.idIdentificacion.value;
    
    for (let i = 0; i < this.listaEmpleados.length; i++) {

      if(this.listaEmpleados[i].identificacion_empleado == valueIdentificacion && this.listaEmpleados[i].id_identificacion == this.idTipoIdentificacion){     
        this.toastr.error('La identificación la esta registrada', 'Fallo registro'); 

        this.form = this.formBuilder.group({         
          identificacionEmpleado : [''],
        }); 

      }  

  }

}

}

var date = new Date();