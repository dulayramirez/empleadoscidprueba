import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { beIdentificacion } from 'src/app/models/interfaces.index';
import { appSettings } from "src/app/common/utils/constants";

@Injectable({
  providedIn: 'root'
})
export class IdentificacionService {
  
  constructor(private http:HttpClient) { 
    
  }
 
  getIdentificacioniones(): Observable<beIdentificacion> {
    return this.http.get<beIdentificacion>(`${appSettings.url_api_empleados}/Identificacion/ListarIdentificacion`);
  }

  getIdentificacion(idIdentificacion : number): Observable<beIdentificacion> {
    return this.http.get<beIdentificacion>(`${appSettings.url_api_empleados}/Identificacion/SeleccionarIdentificacion?id_Identificacion=${idIdentificacion}`);
  }
 
}
