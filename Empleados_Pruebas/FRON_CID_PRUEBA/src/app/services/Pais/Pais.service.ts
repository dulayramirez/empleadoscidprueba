import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { bePais } from 'src/app/models/interfaces.index';
import { appSettings } from "src/app/common/utils/constants";

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  constructor(private http:HttpClient) { 
    
  }
 
  getPaises(): Observable<bePais> {
    return this.http.get<bePais>(`${appSettings.url_api_empleados}/Pais/ListarPais`);
  }

  getPais(idPais : number): Observable<bePais> {
    return this.http.get<bePais>(`${appSettings.url_api_empleados}/Pais/SeleccionarPais?id_Pais=${idPais}`);
  }

}
