import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { beCorreo } from 'src/app/models/interfaces.index';
import { appSettings } from "src/app/common/utils/constants";

@Injectable({
  providedIn: 'root'
})
export class CorreosService {

  constructor(private http:HttpClient) { 
    
  }
 
  getCorreos(): Observable<beCorreo> {
    return this.http.get<beCorreo>(`${appSettings.url_api_empleados}/Correo/ListarCorreo`);
  }

  getCorreo(idCorreo : number): Observable<beCorreo> {
    return this.http.get<beCorreo>(`${appSettings.url_api_empleados}/Correo/SeleccionarCorreo?id_Correo=${idCorreo}`);
  }

}
