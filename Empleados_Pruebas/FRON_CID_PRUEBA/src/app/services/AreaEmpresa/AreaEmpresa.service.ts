import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { beAreaEmpresa } from 'src/app/models/interfaces.index';
import { appSettings } from "src/app/common/utils/constants";

@Injectable({
  providedIn: 'root'
})
export class AreaEmpresaService {

  constructor(private http:HttpClient) { 
    
  }
 
  getAreaEmpresas(): Observable<beAreaEmpresa> {
    return this.http.get<beAreaEmpresa>(`${appSettings.url_api_empleados}/AreaEmpresa/ListarAreaEmpresa`);
  }

  getAreaEmpresa(idEmpresa:number): Observable<beAreaEmpresa> {
    return this.http.get<beAreaEmpresa>(`${appSettings.url_api_empleados}/AreaEmpresa/SeleccionarAreaEmpresa?id_AreaEmpresa=${idEmpresa}`);
  }


}
