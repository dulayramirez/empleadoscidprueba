import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { beEstado } from 'src/app/models/interfaces.index';
import { appSettings } from "src/app/common/utils/constants";

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  constructor(private http:HttpClient) { 
    
  }
 
  getEstados(): Observable<beEstado> {
    return this.http.get<beEstado>(`${appSettings.url_api_empleados}/Estado/ListarEstado`);
  }

  getEstado(idEstado : number): Observable<beEstado> {
    return this.http.get<beEstado>(`${appSettings.url_api_empleados}/Estado/SeleccionarEstado?id_Estado=${idEstado}`);
  }
 
}
