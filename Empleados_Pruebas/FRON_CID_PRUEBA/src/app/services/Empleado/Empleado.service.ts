import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { beEmpleado } from 'src/app/models/interfaces.index';
import { appSettings } from "src/app/common/utils/constants";

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  constructor(private http:HttpClient) { 
    
  }
   
  getEmpleados(): Observable<beEmpleado> {
    return this.http.get<beEmpleado>(`${appSettings.url_api_empleados}/Empleado/ListarEmpleados`);
  }

  getEmpleado(idEmpleado:number): Observable<beEmpleado> {
    return this.http.get<beEmpleado>(`${appSettings.url_api_empleados}/Empleado/SeleccionarEmpleado?id_empleado=${idEmpleado}`);
  }

  postEmpleado(Empleado:any): Observable<beEmpleado>{
    return this.http.post<beEmpleado>(`${appSettings.url_api_empleados}/Empleado/AdicionarEmpleado`, Empleado);
  }

  putEmpleado(Empleado:any): Observable<beEmpleado>{
    return this.http.put<beEmpleado>(`${appSettings.url_api_empleados}/Empleado/EditarEmpleado`, Empleado);
  }

  deleteEmpleado(idEmpleado:number): Observable<beEmpleado>{
    return this.http.delete<beEmpleado>(`${appSettings.url_api_empleados}/Empleado/EliminaEmpleado?id_empleado=${idEmpleado}`);
  }

}
