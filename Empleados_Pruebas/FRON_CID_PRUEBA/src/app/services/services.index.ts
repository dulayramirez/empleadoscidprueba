export { AreaEmpresaService } from './AreaEmpresa/AreaEmpresa.service';
export { CorreosService } from './Correos/Correos.service';
export { EmpleadoService } from './Empleado/Empleado.service';
export { EstadoService } from './Estado/Estado.service';
export { IdentificacionService } from './Identificacion/Identificacion.service';
export { PaisService } from './Pais/Pais.service';